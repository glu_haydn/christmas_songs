This is a test project for GitLab University. 

My three favourite Chistmas Songs:

1. [Do they know it's Christmas] (https://en.wikipedia.org/wiki/Do_They_Know_It%27s_Christmas%3F) of which there are three versions:
  1. <a href="http://www.youtube.com/watch?feature=player_embedded&v=Zc8TYsYb5i0
" target="_blank"><img src="http://img.youtube.com/vi/Zc8TYsYb5i0/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="144" height="108" border="10" /></a>
  1. <a href="http://www.youtube.com/watch?feature=player_embedded&v=H5vMdmajxFY
" target="_blank"><img src="http://img.youtube.com/vi/H5vMdmajxFY/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="144" height="108" border="10" /></a>
  1. <a href="http://www.youtube.com/watch?feature=player_embedded&v=-w7jyVHocTk
" target="_blank"><img src="http://img.youtube.com/vi/-w7jyVHocTk/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="144" height="108" border="10" /></a>

1. [You're A Mean One, Mr. Grinch] (https://en.wikipedia.org/wiki/You%27re_a_Mean_One,_Mr._Grinch)

<a href="http://www.youtube.com/watch?feature=player_embedded&v=AFUcLZExzlA
" target="_blank"><img src="http://img.youtube.com/vi/AFUcLZExzlA/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="144" height="108" border="10" /></a>

1. [Let It Snow] (https://en.wikipedia.org/wiki/Let_It_Snow!_Let_It_Snow!_Let_It_Snow!)

<a href="http://www.youtube.com/watch?feature=player_embedded&v=RWTQqpYBHQ8
" target="_blank"><img src="http://img.youtube.com/vi/RWTQqpYBHQ8/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="144" height="108" border="10" /></a>

